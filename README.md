# Dark Crystal Book 

DEPRECATED in favor of our website [https://darkcrystal.pw](https://darkcrystal.pw)

Simple website containing dark-crystal protocol specification and other documents

[available for historical purposes on gitlab pages](https://dark-crystal-javascript.gitlab.io)
